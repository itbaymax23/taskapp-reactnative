import rootReducers from '../reduce';
import {autoRehydrate,persistStore} from 'redux-persist-immutable';
import createActionBuffer from 'redux-action-buffer';
import {REHYDRATE} from 'redux-persist/constants';
import Immutable from 'immutable';
import {applyMiddleware,compose,createStore} from 'redux';
import {AsyncStorage} from 'react-native';
import createSagaMiddleware from 'redux-saga';
const initialstate = new Immutable.Map({
    login:Immutable.Map({
        isLoggedIn:false
    })
});

export default function configurestore()
{
    const sagamiddleware = createSagaMiddleware();

    const store = createStore(
        rootReducers,
        initialstate,
        compose(applyMiddleware(sagamiddleware,createActionBuffer(REHYDRATE)),autoRehydrate({log:true}))
    );

    persistStore(
        store,
        {
            storage:AsyncStorage,
            key:"user"
        }
    );

    return {
        ...store,runSaga:[]
    }
}


