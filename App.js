import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {Provider} from 'react-redux';
import ConfigureStore from './store/configurestore';
import Navigator from './components/Navigation';
import * as Font from 'expo-font';

export default function App() {
  console.disableYellowBox = true;
  const store = ConfigureStore();
  return (
    <Provider store={store}>
      <Navigator/>
    </Provider>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  },
});
