import * as actions from '../actions/action-type';

export default function loginReducers(state,action={})
{
    switch(action.type)
    {
        case actions.LOGIN_SUCCESS:
            return state.withMutations(state=>state.set("user",action.user));       
    }

    return state;
}
