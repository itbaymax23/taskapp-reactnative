import {combineReducers} from 'redux-immutable';
import LoginReducer from './loginReducers';

export default combineReducers({login:LoginReducer});