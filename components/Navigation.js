import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';
import {FirstPage,SelectUserType,Register,RegisterProfile,Verify,OfficialDocument, VerifyIdentity, SelfieTime, YourLocation, Confirm, Success,Main,Tasker} from './Page';

const stacknavigator = createStackNavigator({
    FirstPage:{
        screen:FirstPage,
        navigationOptions:{
            header:()=>null
        }
    },
    SelectUserType:{
        screen:SelectUserType,
        navigationOptions:{
            header:()=>null
        }
    },
    Register:{
        screen:Register,
        navigationOptions:{
            header:()=>null
        }
    },
    RegisterProfile:{
        screen:RegisterProfile,
        navigationOptions:{
            header:()=>null
        }
    },
    Verify:{
        screen:Verify,
        navigationOptions:{
            header:()=>null
        }
    },
    OfficialDocument:{
        screen:OfficialDocument,
        navigationOptions:{
            header:()=>null
        }
    },
    VerifyIdentity:{
        screen:VerifyIdentity,
        navigationOptions:{
            header:()=>null
        }
    },
    SelfieTime:{
        screen:SelfieTime,
        navigationOptions:{
            header:()=>null
        }
    },
    YourLocation:{
        screen:YourLocation,
        navigationOptions:{
            header:()=>null
        }
    },
    Confirm:{
        screen:Confirm,
        navigationOptions:{
            header:()=>null
        }
    },
    Success:{
        screen:Success,
        navigationOptions:{
            header:()=>null
        }
    },
    Main:{
        screen:Main,
        navigationOptions:{
            header:()=>null
        }
    },
    Tasker:{
        screen:Tasker,
        navigationOptions:{
            header:()=>null
        }
    }
})

let App =  createAppContainer(stacknavigator);

class AppContainer extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            font:false
        }
    }

    async componentDidMount()
    {
        await Font.loadAsync({
            Arial:{
                uri:require('../assets/font/Arial.ttf')
            }
        });

        this.setState({
            font:true
        })
    }

    render()
    {
        if(!this.state.font)
        {
            return <AppLoading/>; 
        }
        else
        {
            return <App/>;
        }
    }
}


export default AppContainer;