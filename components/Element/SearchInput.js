import React from 'react';
import {View,StyleSheet,TextInput} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
const SearchInput = ({ placeholder}) => {
  return (
    <View style={style.container}>
        <TextInput style={style.text} placeholder={placeholder} placeholderTextColor="#08223980"></TextInput>
        <Ionicons name="ios-search" style={style.icon}></Ionicons>
    </View>
  )
}


const style = StyleSheet.create({
    container:{
        flexDirection:'row',
        borderColor:'#0822390E',
        padding:5,
        flex:1,
        borderWidth:1,
        alignItems:'center'
    },
    icon:{
        marginLeft:'auto',
        fontSize:hp('3%')
    },
    text:{
        fontSize:hp('2%'),
        paddingLeft:10,
        flex:1,
        fontFamily:"Arial"
    }
})

export default SearchInput;