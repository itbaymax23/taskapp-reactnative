import React from 'react';
import {ScrollView,StyleSheet,TouchableOpacity,Text,View} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Entypo} from '@expo/vector-icons';

class Select extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:false,
            selecteditem:""
        }
    }

    select = () => {
        let selected = !this.state.selected;
        this.setState({
            selected:selected
        })
    }

    selectitem = (item) => {
        if(this.state.selecteditem == item)
        {
            item = "";
        }
        
        this.setState({
            selecteditem:item,
            selected:false
        })
    }
    render()
    {
        return (
            <View>
                <TouchableOpacity style={style.select} onPress={this.select}>
                    <Text style={style.selectedtext}>{this.state.selecteditem?this.state.selecteditem:this.props.label}</Text>
                    <Entypo style={style.selecticon} name={this.state.selected?"chevron-thin-down":"chevron-thin-right"}></Entypo>
                </TouchableOpacity>
                {
                    this.state.selected && (
                        <ScrollView style={{flex:1,zIndex:10}}>
                            {
                                this.props.selecteditems.map(item=>{
                                    return (
                                        <TouchableOpacity style={this.state.selecteditem == item?style.selected:style.select} onPress={()=>this.selectitem(item)}>
                                            <Text style={this.state.selecteditem == item?style.selecteditem:style.selectedtext}>{item}</Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </ScrollView>
                    )
                }
            </View>
        )
    }
}

const style = StyleSheet.create({
    select:{
        width:wp('100%'),
        backgroundColor:'#FFFFFF1D',
        paddingLeft:wp('8%'),
        paddingRight:wp('5%'),
        paddingTop:hp('1.3%'),
        paddingBottom:hp('1.3%'),
        position:'relative',
        marginTop:1,
        flexDirection:'row'
    },
    selectedtext:{
        fontSize:hp('2.4%'),
        fontWeight:'500',
        color:'#FFFFFFF4',
        fontFamily:"Arial"
    },
    selecticon:{
        fontSize:hp('3.5%'),
        color:'#FFFFFFF4',
        marginLeft:'auto',
        fontFamily:"Arial"
    },
    selected:{
        width:wp('100%'),
        backgroundColor:'#FFFFFFF4',
        paddingLeft:wp('8%'),
        paddingRight:wp('5%'),
        paddingTop:hp('1.3%'),
        paddingBottom:hp('1.3%'),
        position:'relative',
        marginTop:1,
        flexDirection:'row'
    },
    selecteditem:{
        fontSize:hp('2.4%'),
        fontWeight:'500',
        color:'#324A5D',
        fontFamily:"Arial"
    }
})

export default Select;