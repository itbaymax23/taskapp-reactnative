import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

import Style from '../style';

const Button = ({ onPress, children, style,icon,color }) => {
  let buttonstyle = JSON.parse(JSON.stringify(Style.button));
  if(color)
  {
    buttonstyle.backgroundColor = color;
  }
  return (
    <TouchableOpacity onPress={onPress} style={buttonstyle}>
      {icon}
      <Text style={style}>{ children }</Text>
    </TouchableOpacity>
  )
}

export default Button;