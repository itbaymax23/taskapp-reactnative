import React from 'react';
import {View,TouchableOpacity,StyleSheet,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {AntDesign} from '@expo/vector-icons';
import Collapsible from 'react-native-collapsible';

import Style from '../style';
import * as Color from '../colors';
class TaskType extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:false
        }
    }

    select = () => {
        let selected = !this.state.selected;
        this.setState({
            selected:selected
        })
    }

    render()
    {
        return(
            <View style={{flex:1}}>
                <TouchableOpacity style={style.container} onPress={this.select}>
                    <Text style={Style.jobtitle}>{this.props.title}</Text>
                    <AntDesign name={this.state.selected?"plus":"minus"} style={{color:this.state.selected?Color.activecolor:Color.deactivecolor,...style.icon}}></AntDesign>
                </TouchableOpacity>
                <Collapsible collapsed={!this.state.selected}>
                    {
                        this.props.children
                    }
                </Collapsible>
            </View>
            
        )
    }
}

const style = StyleSheet.create(
    {
        container:{
            flexDirection:'row',
            paddingLeft:wp('9%'),
            paddingRight:wp('9%'),
            paddingTop:hp('2%'),
            paddingBottom:hp('2%'),
            alignItems:'center',
            backgroundColor:'white',
            marginBottom:1,
            display:'flex'
        },
        icon:{
            fontSize:hp('4%'),
            marginLeft:'auto'
        }
    }
)

export default TaskType;