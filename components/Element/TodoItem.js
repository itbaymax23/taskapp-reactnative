import React from 'react';
import {Text, View,TouchableOpacity} from 'react-native';

import {AntDesign} from '@expo/vector-icons';
import Style from '../style';

const TodoItem = ({ onPress, todo }) => {
  return (
    <View style={Style.todoitem}>
        <Text style={Style.todoitem_text}>{todo}</Text>
        <TouchableOpacity onPress={onPress}>
            <AntDesign style={Style.todoitem_close} name="close"></AntDesign>
        </TouchableOpacity>
    </View>
  )
}

export default TodoItem;