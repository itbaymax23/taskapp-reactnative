import React from 'react';
import {View,Text,TextInput,StyleSheet} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Ionicons} from '@expo/vector-icons';
import {EvilIcons} from '@expo/vector-icons';
class InputValue extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            focused:false,
            text:""
        }
    }

    focus = () => {
        this.setState({
            focused:true
        })
    }

    handletext = (text) => {
        this.setState({
            text:text
        })
    }

    onblur = () => {
        this.setState({
            focused:false
        })

        this.props.onChange(this.state.text);
    }
    render()
    {   
        let inputstyle = style.inputfield;
        let iconstyle = style.icon;
        if(this.state.focused)
        {
            inputstyle = style.inputfieldfocused;
            iconstyle = style.iconfocused; 
        }

        if(this.props.error)
        {
            inputstyle = style.inputfieldfocused;
            iconstyle = style.iconerror;
        }
        return (
            <View style={style.inputcontainer}>
                {
                    (this.state.focused || this.props.error) && (
                        <Text style={style.label}>{this.props.label}</Text>
                    )
                }
                <View style={{borderTopWidth:this.props.bordertop?1:0,...inputstyle}}>
                    {
                        this.props.icon && (
                            <Ionicons name={this.props.icon} style={iconstyle}></Ionicons>
                        )
                    }
                    <TextInput onBlur={this.onblur} secureTextEntry={this.props.type == 'password'} onFocus={this.focus} style={{flex:1,paddingLeft:wp('3%'),fontSize:hp('2.3%'),color:this.state.focused?"black":"white"}} placeholder={this.props.error?this.props.error:this.props.label} placeholderTextColor={this.props.error?'red':"#FFFFFFE0"} onChangeText={(text)=>this.handletext(text)} value={this.state.text}></TextInput>
                    {
                        (this.props.success && !this.state.focused) &&(
                            <EvilIcons name="check" style={style.icon}></EvilIcons>
                        )
                    }
                    {
                        this.props.error && (
                            <Ionicons name="md-information-circle-outline" style={style.iconerror}></Ionicons>
                        )
                    }
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    inputcontainer:{
        width:wp('90%')
    },
    focusedinput:{
        backgroundColor:'white',
        flexDirection:'row'
    },
    label:{
        color:'white',
        fontSize:hp('2.3%'),
        marginBottom:hp('1%'),
        marginTop:hp('2%'),
        fontFamily:"Arial"
    },
    inputfield:{
        borderBottomColor:'#FFFFFFE0',
        borderBottomWidth:1,
        borderTopColor:'#FFFFFFE0',
        flexDirection:'row',
        padding:10
    },
    inputfieldfocused:{
        backgroundColor:'white',
        flexDirection:'row',
        padding:10,
        borderTopLeftRadius:10,
        borderTopRightRadius:10
    },
    icon:{
        color:'#FFFFFF80',
        fontSize:hp('4%')
    },
    iconfocused:{
        color:'#53B9D1',
        fontSize:hp('4%')
    },
    iconerror:{
        color:'red',
        fontSize:hp('4%')
    }
})
export default InputValue;