import React from 'react';
import {View,StyleSheet,TextInput} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as Color from '../colors';
import Style from '../style';
import {Ionicons} from '@expo/vector-icons';
class TextArea extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            focus:false,
            text:""
        }
    }

    onfocus = () => {
        let focus = !this.state.focus;
        this.setState({
            focus:focus
        })
    }

    handlechange = (text)=> {
        this.setState({
            text:text
        })
    }

    onblur = () => {
        console.log(this.state.text);
        this.setState({
            focus:false
        })
        this.props.handlechange(this.state.text);
    }

    render()
    {
        return (
            <View style={{...style.container,borderColor:this.state.focus?Color.deactivestar:Color.activecolor}}>
                <TextInput 
                style={{...Style.description,color:this.state.focus?"#7D8A95":"#384D5F"}} 
                placeholder={this.props.placeholder} 
                placeholderTextColor={Color.deactivestar}
                onFocus={this.onfocus}
                onBlur={this.onblur}
                onChangeText={(text)=>this.handlechange(text)}
                multiline={this.props.numberofline?true:false}
                numberOfLines={this.props.numberofline?this.props.numberofline:1}
                value={this.state.text}
                ></TextInput>
                {
                    (this.props.success && !this.state.focus) && (
                        <Ionicons name="ios-checkmark-circle-outline" style={style.icon}></Ionicons>
                    )
                }
            </View>
        )
    }
}

const style = StyleSheet.create({
    container:{
        paddingLeft:wp('6%'),
        paddingRight:wp('6%'),
        paddingTop:hp('1.2%'),
        paddingBottom:hp('1.2%'),
        borderWidth:1,
        borderRadius:5
    },
    icon:{
        position:'absolute',
        right:wp('6%'),
        bottom:hp('1.2%'),
        color:'#4EA9B6',
        fontSize:hp('3%')
    }
})

export default TextArea;