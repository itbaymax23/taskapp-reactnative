import {Ionicons} from '@expo/vector-icons';
import Style from '../style';
import * as Color from '../colors';
import React from 'react';
export function validateEmail(email)
{
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

export function getreview(review)
{
    let reviewelement = [];

    for(let item = 0;item < review;item++)
    {
        reviewelement.push(<Ionicons key={item} name="ios-star" style={{color:Color.activestar,...Style.star}}></Ionicons>);
    }

    for(let item = review;item<5;item++)
    {
        reviewelement.push(<Ionicons key={item} name="ios-star" style={{color:Color.deactivecolor,...Style.star}}></Ionicons>);
    }

    return reviewelement;
}

