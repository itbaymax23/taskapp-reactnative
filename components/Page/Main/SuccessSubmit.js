import React from 'react';
import {View,Image,StyleSheet,Text} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';
import {ProposalLayout} from '../../Layout';


class SuccessSubmit extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <ProposalLayout title="Confirmation" backdisable={true}>
                <Image source={require('../../../assets/icons/success.png')} style={style.image}></Image>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>Proposal Submitted</Text>
                </View>
                <View style={{flex:1,alignItems:'center',marginTop:hp('7%')}}>
                    <Text style={{...Style.jobdescription,textAlign:'center'}}>
                        I need help caring for my dog {'\n'}
                        Your Bid is 300 $
                    </Text>
                </View>
                <View style={{alignItems:'center',flex:1,marginTop:hp('5%'),marginBottom:hp('2%')}}>
                    <Button color={Color.deactivecolor} style={Style.buttontext1} onPress={()=>this.props.navigation.navigate("JobList")}>Done</Button>
                </View>
            </ProposalLayout>
        )
    }
}

const style = StyleSheet.create({
    image:{
        width:hp('14%'),
        height:hp('19%'),
        marginTop:hp('7%'),
        marginBottom:hp('7%'),
        alignSelf:'center'
    }
})
export default SuccessSubmit;