import React from 'react';
import {ProposalLayout} from '../../Layout';
import { Text,View,StyleSheet,Image,TouchableOpacity } from 'react-native';
import {Entypo} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';
import * as Util from '../../Tools/Utils';

class JobItem extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data:{
                title:"I need help caring for my dog",
                description:"Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
                review:5,
                reviewcount:3,
                type:"fixed",
                amount:200,
                client:{
                    status:"online",
                    name:"Tarek Ayoubi",
                    address:"United States of America, Los Angeles",
                    register:"Dec 28, 2018"
                }
            }
        }
    }

    showclient = () => {
        this.props.navigation.navigate('ClientDetail');
    }

    render()
    {
        return (
            <ProposalLayout title="Description" {...this.props}>
                <View style={style.item}>
                    <Text style={Style.title1}>{this.state.data.title}</Text>
                    <View style={style.itemdetail}>
                        <Text style={style.itemtime}>7 mins ago</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={Style.jobdescription}>{this.state.data.description}</Text>
                </View>
                <View style={style.item}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1,flexDirection:'row'}}>
                            {Util.getreview(this.state.data.review)}
                            <Text style={{...Style.jobdescription,marginLeft:10}}>({this.state.data.reviewcount})</Text>
                        </View>
                    </View>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                            <Image source={this.state.data.type == 'fixed'?require('../../../assets/icons/fixed.png'):require('../../../assets/icons/clock.png')}></Image>
                            <Text style={{...Style.jobdescription,marginLeft:10}}>{this.state.data.type == 'fixed'?"Fixed":"Hourly"}</Text>
                        </View>
                        <View style={{flex:1,flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                            <Image source={require('../../../assets/icons/money.png')}></Image>
                            <Text style={{...Style.jobdescription,marginLeft:10}}>{this.state.data.amount} {this.state.data.type == 'fixed'?"$":"$/hr"}</Text>
                        </View>
                    </View>
                </View>
                <View style={style.itemcover}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Text style={{position:"absolute",color:this.state.data.client.status == 'online'?Color.activecolor:Color.deactivestar}}>
                            {this.state.data.client.status == 'online'?'Online':"a mins ago"}
                        </Text>
                        <View style={style.profilecontainer}>
                            <TouchableOpacity style={style.profile}>
                                <Entypo style={style.profileicon} name="user"></Entypo>
                            </TouchableOpacity>
                            <TouchableOpacity style={{backgroundColor:this.state.data.client.status == 'online'?Color.activecolor:Color.deactivestar,...style.onlineicon}}></TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'center',marginTop:hp('1%')}}>
                        <Text style={Style.title1}>{this.state.data.client.name}</Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center',marginTop:hp('1%'),flexDirection:'row'}}>
                        <Entypo name="location-pin" style={Style.locationpin}></Entypo>
                        <View style={{marginLeft:10}}>
                            <Text style={Style.locationtext}>{this.state.data.client.address}</Text>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'center',marginTop:hp('1%')}}>
                        <Text style={Style.locationtext}>Member since {this.state.data.client.register}</Text>
                    </View>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center',marginTop:hp('1%')}}>
                        <TouchableOpacity onPress={this.showclient}>
                            <Text style={{color:Color.activecolor,fontSize:hp('2.4%')}}>See more ></Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{marginTop:hp('3%'),alignItems:'center'}}>
                    <Button onPress={()=>this.props.navigation.navigate('JobAccept')} style={Style.buttontext1} color={Color.deactivecolor}>Submit Proposal</Button>
                </View>
            </ProposalLayout>
        )
    }
}

const style = StyleSheet.create({
    itemtime:{
        fontSize:hp('1.4%'),
        color:Color.deactivecolor,
        fontFamily:"Arial"
    },
    itemdetail:{
        marginLeft:'auto',
        alignItems:'center'
    },  
    item:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%'),
        flexDirection:'row',
        alignItems:'center'
    },
    profilecontainer:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        marginLeft:'auto',
        marginRight:'auto'
    },
    profile:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        overflow:'hidden',
        backgroundColor:'#EDEFEF'
    },
    profileicon:{
        fontSize:hp('8%'),
        color:Color.activecolor
    },
    onlineicon:{
        width:hp('1%'),
        height:hp('1%'),
        borderRadius:hp('0.5%'),
        position:'absolute',
        right:0,
        bottom:hp('1%')
    }, 
    textinput:{
        width:wp('33.3%'),
        borderRadius:5,
        borderColor:'#384D5F',
        borderWidth:1,
        flexDirection:'row',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:5,
        paddingBottom:5
    },
    itemcover:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%')
    }
})
export default JobItem;