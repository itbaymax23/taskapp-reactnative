import React from 'react';
import {JobLayout,HomeLayout} from '../../Layout';
import { Text,View,StyleSheet } from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Style from '../../style';
import * as Color from '../../colors';

class JobList extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            joblist:[
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",recent:true,type:"fixed",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"30",type:"hourly",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",type:"fixed",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",type:"fixed",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",type:"fixed",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",type:"fixed",reviewcount:13},
                {review:5,title:'I need help caring for my dog',description:"Lorem Ipsum is simply dummy text of the printing and typesetting industry .......",amount:"200",type:"fixed",reviewcount:13}
            ]
        }
    }

    gettitle = () => {
        return (
            <View style={Style.middle}>
                <Text style={Style.title1}>15 New Tasks</Text>
                <View style={{marginLeft:'auto',flexDirection:'row',alignItems:'center'}}>
                    <Text style={Style.title1}>Filter</Text>
                    <MaterialIcons name="tune" style={style.icon}></MaterialIcons>
                </View>
            </View>
        )
    }

    selectjob = () => {
        this.props.navigation.navigate('JobItem');
    }

    render()
    {
        return (
            <HomeLayout title={this.gettitle()} placeholder="Find job">
                {
                    this.state.joblist.map((row,index)=>{
                        return (
                            <JobLayout key={index} data={row} onPressed={this.selectjob}></JobLayout>
                        )
                    })
                }
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        fontSize:hp('2.5%'),
        marginLeft:wp('2%'),
        color:Color.deactivecolor
    }
})
export default JobList;