import React from 'react';
import {View,StyleSheet,TouchableOpacity,Text,Image} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {HomeLayout} from '../../Layout';
import * as Colors from '../../colors';
import Style from '../../style';
import {Entypo} from '@expo/vector-icons';

class Home extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:""
        }
    }

    image = [
        {image:require('../../../assets/category/Animals.jpg'),text:"Animals"},
        {image:require('../../../assets/category/Babysitter.jpg'),text:"Babysitter"},
        {image:require('../../../assets/category/Cleaning.jpg'),text:"Cleaning"},
        {image:require('../../../assets/category/Event.jpg'),text:"Event"},
        {image:require('../../../assets/category/Gardening.jpg'),text:"Gardening"},
        {image:require('../../../assets/category/Handyman.jpg'),text:"Handyman"}
    ];

    onSelect = (item) => {
        this.setState({
            selected:item
        })
    }

    showjoblist = () => {
        this.props.navigation.navigate('JobList');
    }

    render()
    {
        return (
            <HomeLayout placeholder="Find Task">
                <View style={Style.container}>
                    {
                        this.image.map((row,index)=>{
                            return (
                                <View style={style.item} key={index}>
                                    <TouchableOpacity style={style.itemcontent} onPress={()=>this.onSelect(row.text)}>
                                        <Image style={style.categoryimg} source={row.image}></Image>
                                        <View style={{flex:1,justifyContent:'center'}}>
                                            <Text style={{color:this.state.selected == row.text?Colors.activecolor:Colors.deactivecolor,...style.itemtext}}>{row.text}</Text>
                                        </View>
                                        <Text style={{color:this.state.selected == row.text?Colors.activecolor:Colors.deactivecolor,...style.itembadge}}>
                                            New {'\n'} + 15
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.showjoblist} style={{backgroundColor:this.state.selected == row.text?Colors.activecolor:"white",...style.itembtn}}>
                                        <Entypo style={{color:this.state.selected == row.text?"white":"#808E99",...style.iconright}} name="chevron-thin-right"></Entypo>
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </View>
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    item:{
        flexDirection:'row',
        marginBottom:5,
        flex:1
    },
    itemcontent:{
        flexDirection:'row',
        padding:3,
        flex:1,
        backgroundColor:'white'
    },
    categoryimg:{
        width:hp('15%'),
        height:hp('12%'),
        marginRight:15
    },
    itemtext:{
        fontSize:hp('3%'),
        fontWeight:'bold',
        fontFamily:"Arial"
    },
    itembadge:{
        fontSize:hp('2%'),
        fontWeight:'bold',
        textAlign:'center',
        fontFamily:"Arial"
    },
    itembtn:{
        width:wp('14%'),
        marginLeft:5,
        justifyContent:'center',
        alignItems:'center'
    },
    iconright:{
        fontSize:hp('4%')
    }
})

export default Home;