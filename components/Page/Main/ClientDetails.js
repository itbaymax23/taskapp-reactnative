import React from 'react';
import {ProposalLayout} from '../../Layout';
import { Text,View,StyleSheet,Image,TouchableOpacity } from 'react-native';
import {Entypo} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';
import * as Util from '../../Tools/Utils';

class ClientDetails extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            client:{
                status:"online",
                name:"Tarek Ayoubi",
                address:"United States of America, Los Angeles",
                register:"Dec 28, 2018",
                postedjob:5,
                openjob:1,
                activejob:1,
                review:5,
                totalspent:800,
                avg:34,
                totalhour:32,
                hire:78
            }
        }
    }

    
    render()
    {
        return (
            <ProposalLayout title="Description" {...this.props}>
                <View style={style.itemcover}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <Text style={{position:"absolute",color:this.state.client.status == 'online'?Color.activecolor:Color.deactivestar}}>
                            {this.state.client.status == 'online'?'Online':"a mins ago"}
                        </Text>
                        <View style={style.profilecontainer}>
                            <TouchableOpacity style={style.profile}>
                                <Entypo style={style.profileicon} name="user"></Entypo>
                            </TouchableOpacity>
                            <TouchableOpacity style={{backgroundColor:this.state.client.status == 'online'?Color.activecolor:Color.deactivestar,...style.onlineicon}}></TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'center',marginTop:hp('1%')}}>
                        <Text style={Style.title1}>{this.state.client.name}</Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center',marginTop:hp('1%'),flexDirection:'row'}}>
                        <Entypo name="location-pin" style={Style.locationpin}></Entypo>
                        <View style={{marginLeft:10}}>
                            <Text style={Style.locationtext}>{this.state.client.address}</Text>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'center',marginTop:hp('1%')}}>
                        <Text style={Style.locationtext}>Member since {this.state.client.register}</Text>
                    </View>
                </View>
                <View style={{flex:1,marginBottom:2,flexDirection:'row'}}>
                    <View style={{...style.itemcover,marginRight:1,alignItems:'center'}}>
                        <AnimatedCircularProgress
                        width={5} 
                        fill={this.state.client.review / 5 * 100} 
                        backgroundColor="#08223919" 
                        tintColor='#4EA9B6'
                        size={hp('9%')} 
                        >
                            {()=>(
                                <Text style={{...Style.progress_txt,color:Color.activecolor}}>{this.state.client.review / 5 * 100} %</Text>
                            )}
                        </AnimatedCircularProgress>
                        <Text style={{...Style.defaulttext,marginTop:hp('1%')}}>Positive Reviews</Text>
                    </View>
                    <View style={{...style.itemcover,marginLeft:1,alignItems:'center'}}>
                        <View style={{flex:1,height:hp('9%'),alignItems:'center',justifyContent:'center'}}>
                            <Text style={style.itemtext}>{this.state.client.postedjob}</Text>
                        </View>
                        <Text style={Style.defaulttext}>Job Posted</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Open Jobs</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.openjob}</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Active Jobs</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.activejob}</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Total Spent</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>Over {this.state.client.totalspent} $</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Avg Hourly Pay</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.avg} $/hr</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Total Hours</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.totalhour} hours</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Hire</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.hire} %</Text>
                </View>
            </ProposalLayout>
        )
    }
}

const style = StyleSheet.create({
    itemtime:{
        fontSize:hp('1.4%'),
        color:Color.deactivecolor,
        fontFamily:"Arial"
    },
    itemdetail:{
        marginLeft:'auto',
        alignItems:'center'
    },  
    itemtext:{
        fontSize:hp('4.5%'),
        color:Color.activecolor,
        fontWeight:'bold',
        fontFamily:"Arial"
    },  
    item:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%'),
        flexDirection:'row',
        alignItems:'center'
    },
    profilecontainer:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        marginLeft:'auto',
        marginRight:'auto'
    },
    profile:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        overflow:'hidden',
        backgroundColor:'#EDEFEF'
    },
    profileicon:{
        fontSize:hp('8%'),
        color:Color.activecolor
    },
    onlineicon:{
        width:hp('1%'),
        height:hp('1%'),
        borderRadius:hp('0.5%'),
        position:'absolute',
        bottom:hp('1.2%'),
        right:0
    }, 
    textinput:{
        width:wp('33.3%'),
        borderRadius:5,
        borderColor:'#384D5F',
        borderWidth:1,
        flexDirection:'row',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:5,
        paddingBottom:5
    },
    itemcover:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%')
    }
})
export default ClientDetails;