import React from 'react';
import {View,Text,StyleSheet,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {MaterialCommunityIcons,Entypo} from '@expo/vector-icons';
import CheckBox from 'react-native-check-box';

import Style from '../../style';
import * as Color from '../../colors';
import {HomeLayout} from '../../Layout';
import { TaskType,Button } from '../../Element';


class JobAccept extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            tasktype:""
        }
    }

    gettitle = () => {
        return (
            <View style={Style.middle}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{marginLeft:'auto',flexDirection:'row',alignItems:'center'}}>
                    <Text style={Style.title1}>Filter</Text>
                    <MaterialCommunityIcons name="tune-vertical" style={{color:Color.activecolor,...style.icon}}></MaterialCommunityIcons>
                </View>
            </View>
        )
    }
    
    onCheck = (item) => {
        if(this.state.tasktype == item)
        {
            item = "";
        }

        this.setState({
            tasktype:item
        })
    }

    onAccept = () => {
        if(this.state.tasktype)
        {
            this.props.navigation.navigate('Proposal');
        }
    }
    render()
    {
        return (
            <HomeLayout placeholder="Find job" title={this.gettitle()}>
                <TaskType title="Task type">
                    <View style={style.content}>
                        <CheckBox 
                        checkBoxColor="#384B5F70" 
                        rightText="Any" 
                        rightTextStyle={{color:this.state.tasktype == "any"?Color.activecolor:Color.deactivestar}}
                        checkedCheckBoxColor="#4EA9B6"
                        isChecked={this.state.tasktype == 'any'}
                        onClick={()=>this.onCheck("any")}
                        style={{flex:1}}                       
                        ></CheckBox>
                        <CheckBox 
                        checkBoxColor="#384B5F70" 
                        rightText="Fixed" 
                        rightTextStyle={{color:this.state.tasktype == "fixed"?Color.activecolor:Color.deactivestar}}
                        checkedCheckBoxColor="#4EA9B6"
                        isChecked={this.state.tasktype == 'fixed'}
                        onClick={()=>this.onCheck("fixed")}                       
                        style={{marginLeft:10,flex:1}}
                        ></CheckBox>
                        <CheckBox 
                        checkBoxColor="#384B5F70" 
                        rightText="Hourly" 
                        rightTextStyle={{color:this.state.tasktype == "hourly"?Color.activecolor:Color.deactivestar}}
                        checkedCheckBoxColor="#4EA9B6"
                        isChecked={this.state.tasktype == 'hourly'}
                        onClick={()=>this.onCheck("hourly")}                       
                        style={{marginLeft:10,flex:1}}
                        ></CheckBox>
                    </View>
                </TaskType>    
                <TaskType title="Client"></TaskType>
                <TaskType title="Number Of Tasks"></TaskType>
                <TaskType title="Hours Pre Week"></TaskType>
                <TaskType title="Budget">
                    <View style={style.content}>
                        {/* <Slider
                            style={{flex:1}}
                            gravity={"center"}
                            min={0}
                            max={1000}
                            step={10}
                            initialLowValue={200}
                            initialHighValue={800}
                            labelTextColor="#4EA9B6"
                            valueType={"number"}
                            textSize={hp('2%')}
                            selectionColor={Color.activecolor}
                            blankColor={Color.deactivestar}
                            labelStyle={{color:'#4EA9B6'}}
                        ></Slider> */}
                    </View>
                </TaskType>
                <TaskType title="Tasks Length"></TaskType>
                <View style={{marginTop:hp('3%'),alignItems:'center',marginBottom:hp('1%')}}>
                    <Button color={this.state.tasktype?Color.activecolor:Color.deactivecolor} style={Style.buttontext1} onPress={this.onAccept}>Accept</Button>
                </View>
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    content:{
        paddingLeft:wp('12%'),
        paddingRight:wp('12%'),
        paddingTop:hp('2.5%'),
        paddingBottom:hp('2.5%'),
        flexDirection:'row',
    },
    icon:{
        fontSize:hp('3%'),
        marginLeft:wp('3%'),
        fontWeight:'bold'
    }
})
export default JobAccept;