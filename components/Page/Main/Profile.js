import React from 'react';
import {ProposalLayout} from '../../Layout';
import { Text,View,StyleSheet,Image,TouchableOpacity } from 'react-native';
import {Entypo, Ionicons,AntDesign} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';
import * as Util from '../../Tools/Utils';

class Profile extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            client:{
                email:"ttkaub@gmail.com",
                name:"Tarek Ayoubi",
                address:"United States of America, Los Angeles",
                register:"Dec 28, 2018",
                postedjob:5,
                openjob:1,
                activejob:1,
                review:5,
                totalspent:800,
                avg:34,
                totalhour:32,
                hire:78
            }
        }
    }

    
    render()
    {
        return (
            <ProposalLayout title="Description" {...this.props}>
                <View style={style.itemcover}>
                    <View style={{flex:1,flexDirection:'row'}}>
                        <View style={style.profilecontainer}>
                            <TouchableOpacity style={style.profile}>
                                <Entypo style={style.profileicon} name="user"></Entypo>
                            </TouchableOpacity>
                            <TouchableOpacity style={{backgroundColor:this.state.client.status == 'online'?Color.activecolor:Color.deactivestar,...style.onlineicon}}></TouchableOpacity>
                        </View>
                    </View>
                    <View style={{flex:1,alignItems:'center',marginTop:hp('1%'),marginBottom:hp('3%')}}>
                        <Text style={Style.title1}>{this.state.client.name}</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Account</Text>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Change Password</Text>
                    <Text style={{...Style.defaulttext,marginLeft:'auto'}}>{this.state.client.email}</Text>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={style.item}>
                        <Text style={Style.defaulttext}>Payment</Text>
                    </View>
                    <TouchableOpacity style={{...style.next,marginLeft:2,marginBottom:2}}>
                        <AntDesign name="right" style={style.icon}></AntDesign>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={style.item}>
                        <Text style={Style.defaulttext}>Locations</Text>
                    </View>
                    <TouchableOpacity style={{...style.next,marginLeft:2,marginBottom:2}}>
                        <AntDesign name="right" style={style.icon}></AntDesign>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={style.item}>
                        <Text style={Style.defaulttext}>Promos</Text>
                    </View>
                    <TouchableOpacity style={{...style.next,marginLeft:2,marginBottom:2}}>
                        <AntDesign name="right" style={style.icon}></AntDesign>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={style.item}>
                        <Text style={Style.defaulttext}>Notifications</Text>
                    </View>
                    <TouchableOpacity style={{...style.next,marginLeft:2,marginBottom:2}}>
                        <AntDesign name="right" style={style.icon}></AntDesign>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={style.item}>
                        <Text style={Style.defaulttext}>Support</Text>
                    </View>
                    <TouchableOpacity style={{...style.next,marginLeft:2,marginBottom:2}}>
                        <AntDesign name="right" style={style.icon}></AntDesign>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:hp('3%'),alignItems:'center'}}>
                    <Button color={Color.deactivecolor} style={Style.buttontext1}>Log Out</Button>
                </View>
            </ProposalLayout>
        )
    }
}

const style = StyleSheet.create({
    itemtime:{
        fontSize:hp('1.4%'),
        color:Color.deactivecolor,
        fontFamily:"Arial"
    },
    itemdetail:{
        marginLeft:'auto',
        alignItems:'center'
    },  
    itemtext:{
        fontSize:hp('4.5%'),
        color:Color.activecolor,
        fontWeight:'bold',
        fontFamily:"Arial"
    },  
    item:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%'),
        flexDirection:'row',
        alignItems:'center'
    },
    profilecontainer:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        marginLeft:'auto',
        marginRight:'auto',
        marginTop:hp('3%')
    },
    profile:{
        width:hp('8%'),
        height:hp('8%'),
        borderRadius:hp('4%'),
        overflow:'hidden',
        backgroundColor:'#EDEFEF'
    },
    profileicon:{
        fontSize:hp('8%'),
        color:Color.activecolor
    },
    icon:{
        fontSize:hp('2.7%'),
        color:'#808E99'
    },
    onlineicon:{
        width:hp('1%'),
        height:hp('1%'),
        borderRadius:hp('0.5%'),
        position:'absolute',
        bottom:hp('1.2%'),
        right:0
    }, 
    textinput:{
        width:wp('33.3%'),
        borderRadius:5,
        borderColor:'#384D5F',
        borderWidth:1,
        flexDirection:'row',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:5,
        paddingBottom:5
    },
    itemcover:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%')
    },
    next:{
        width:wp('12%'),
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'white',
        marginLeft:2
    }
})
export default Profile;