import React from 'react';
import {ProposalLayout} from '../../Layout';
import { Text,View,StyleSheet,TextInput } from 'react-native';
import {MaterialIcons} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';

class Proposal extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data:{
                amount:200,
                type:"fixed",
                price:"",
                description:""
            },
            accepted:false,
            success:false
        }
    }

    onchange= (text) => {
        this.validate(text);
        
        let data = this.state.data;
        data.price = Number(text);
        this.setState({
            data:data
        })
    }

    validate = (price) => {
        if(Number(price) > this.state.data.amount)
        {
            this.setState({
                success:true
            })
        }
        else
        {
            this.setState({
                success:false
            })
        }
    }

    onAccept = () => {
        this.setState({
            accepted:true
        })
    }

    onchangedesc = (text) => {
        let data = this.state.data;
        data.description = text;
        this.setState({
            data:data
        })
    }

    submit = () =>{
        if(this.state.accepted && this.state.data.description)
        {
            this.props.navigation.navigate("SuccessSubmit");
        }
    }
    render()
    {
        return (
            <ProposalLayout title="Terms" {...this.props}>
                <View style={style.item}>
                    <Text style={{...Style.defaulttext,color:this.state.success?'#F0C556':'#384D5F'}}>Client Budget</Text>
                    <View style={style.itemdetail}>
                        <Text style={{...Style.defaulttext,color:this.state.success?'#F0C556':'#384D5F'}}>{this.state.data.amount} {this.state.data.type == 'fixed'?'$':"$/hr"}</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={Style.defaulttext}>Payment</Text>
                    <View style={style.itemdetail}>
                        <Text style={Style.defaulttext}>{this.state.data.type == 'fixed'?'Fixed':"Hourly"}</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={{...Style.defaulttext,color:this.state.success?Color.activecolor:'#384D5F'}}>Service Fee</Text>
                    <View style={style.itemdetail}>
                        <Text style={{...Style.defaulttext,color:this.state.success?Color.activecolor:'#384D5F'}}>15$</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={{...Style.defaulttext,color:this.state.success?Color.activecolor:'#384D5F'}}>You will Receive</Text>
                    <View style={style.itemdetail}>
                        <Text style={{...Style.defaulttext,color:this.state.success?Color.activecolor:'#384D5F'}}>185$</Text>
                    </View>
                </View>
                <View style={style.item}>
                    <Text style={{...Style.defaulttext,color:this.state.success?Color.activecolor:'#384D5F'}}>Your Proposal</Text>
                    <View style={style.itemdetail}>
                        <View style={{...style.textinput,borderColor:!this.state.success?"#384D5F":Color.activecolor}}>
                            <TextInput keyboardType="number-pad" style={{flex:1,textAlign:'center'}} placeholder="Bid" placeholderTextColor="#384D5F" onChangeText={(text)=>this.onchange(text)} value={this.state.data.price}></TextInput>
                            <Text style={{color:!this.state.success?"#384D5F":Color.activecolor,fontSize:hp('2.2%')}}>{this.state.data.type == 'fixed'?" $":" $/hr"}</Text>
                        </View>
                    </View>
                </View>
                {
                    (this.state.success && !this.state.accepted) && (
                        <View style={{flex:1,alignItems:'center',marginTop:hp('2%'),marginBottom:hp('2%')}}>
                            <Button color={Color.activecolor} style={Style.buttontext1} onPress={this.onAccept}>Accept</Button>
                        </View>
                    )
                }
                <View style={style.itemcover}>
                    <Text style={Style.defaulttext}>Cover Letter</Text>
                    <View style={{marginTop:hp('1.5%')}}>
                        <View style={{...Style.covertext,borderColor:!this.state.data.description?"#384D5F":Color.activecolor}}>
                            <TextInput numberOfLines={5} style={{padding:10,color:!this.state.data.description?"#384D5F":Color.activecolor,flex:1}} placeholderTextColor="#384D5F" onChangeText={(text)=>this.onchangedesc(text)} value={this.state.data.description}></TextInput>
                        </View>
                    </View>
                </View>
                
                <View style={{marginTop:hp('3%'),alignItems:'center'}}>
                    <Button style={Style.buttontext1} color={(this.state.accepted && this.state.data.description)?Color.activecolor:Color.deactivecolor} onPress={this.submit}>Submit Proposal</Button>
                </View>
            </ProposalLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        fontSize:hp('2.5%'),
        marginLeft:wp('2%'),
        color:Color.deactivecolor
    },
    item:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%'),
        flexDirection:'row',
        alignItems:'center'
    },
    itemdetail:{
        marginLeft:'auto'
    },
    textinput:{
        width:wp('33.3%'),
        borderRadius:5,
        borderColor:'#384D5F',
        borderWidth:1,
        flexDirection:'row',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:5,
        paddingBottom:5
    },
    itemcover:{
        marginBottom:2,
        flex:1,
        backgroundColor:'white',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1.5%'),
        paddingBottom:hp('1.5%')
    }
})
export default Proposal;