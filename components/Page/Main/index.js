export {default as Home} from './Home';
export {default as JobList} from './JobList';
export {default as JobAccept} from './JobAccept';
export {default as Proposal} from './Proposal';
export {default as JobItem} from './JobItem';
export {default as ClientDetail} from './ClientDetails';
export {default as Profile} from './Profile';
export {default as SuccessSubmit} from './SuccessSubmit';