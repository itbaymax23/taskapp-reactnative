import React from 'react';
import {createAppContainer} from 'react-navigation';
import {Image,Text,StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Ionicons,FontAwesome} from '@expo/vector-icons';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import * as Color from '../colors';
import {Home,JobList,JobAccept,Proposal,JobItem,ClientDetail,Profile,SuccessSubmit} from './Main';
const HomeStack = createStackNavigator({
    Home:{
        screen:Home   
    },
    JobList:{
        screen:JobList
    },
    JobAccept:{
        screen:JobAccept
    },
    Proposal:{
        screen:Proposal
    },
    JobItem:{
        screen:JobItem
    },
    ClientDetail:{
        screen:ClientDetail
    },
    SuccessSubmit:{
        screen:SuccessSubmit
    }
},{
    defaultNavigationOptions:{
        header:()=>null
    }
})

const style = StyleSheet.create({
    icon:{
        fontSize:hp('3%')
    },
    iconimage:{
        width:hp('3%'),
        height:hp('3%')
    },
    icontext:{
        fontSize:hp('1.8%'),
        fontWeight:'500'
    }
})

const App = createBottomTabNavigator({
    Home:{
        screen:HomeStack
    },
    MyJob:{
        screen:Home
    },
    MyClients:{
        screen:Home
    },
    Profile:{
        screen:Profile
    }
},{
    defaultNavigationOptions:({navigation})=>({
            tabBarIcon:({focused,horizontal,tintColor}) => {
                const {routeName} = navigation.state;
                let iconname = "";
                let Iconelement = Ionicons;
                switch(routeName)
                {
                    case 'Home':
                        iconname = "md-home";
                        Iconelement = Ionicons;
                        break;
                    case 'MyClients':
                        iconname = "users";
                        Iconelement = FontAwesome;
                        break;
                    case 'Profile':
                        iconname = "user";
                        Iconelement = FontAwesome;
                        break;
                }

                if(routeName == 'MyJob')
                {
                    return <Image source={require('../../assets/icons/job.png')} style={style.iconimage}></Image>
                }
                else
                {
                    return <Iconelement name={iconname} style={{color:focused?Color.activecolor:Color.icondeactive,...style.icon}}></Iconelement>
                }
            }
        }
    )
});

export default createAppContainer(App);