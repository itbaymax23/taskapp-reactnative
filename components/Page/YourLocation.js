import React from 'react';
import {Image,View,StyleSheet,Text,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Ionicons,Entypo} from '@expo/vector-icons';

import {RegisterLayout} from '../Layout';
import {Button,Select} from '../Element';
import Style from '../style';

class YourLocation extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:"",
            country:["United States of America","Afghanicstan","Aland Islands","Albana","America Samoa","Andora","Angola"],
            city:["Callifornia","London","Washington","Paris","Madrid"]
        }
    }

    onPress = () => {
        this.props.navigation.navigate("Confirm");
    }
    render()
    {
        return (
            <RegisterLayout  {...this.props} title={"Verify Your Identity"} paddingdisabled={true}>
                <Ionicons style={style.icon} name="md-pin"></Ionicons>
                <View style={{flex:1}}>
                    <Select label="Select Your Country" selecteditems={this.state.country}></Select>
                    <Select label="Select Your City" selecteditems={this.state.city}></Select>
                </View>
                <View style={{flex:1,marginTop:hp('20%'),justifyContent:'center',flexDirection:'row'}}>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Next</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        fontSize:hp('20%'),
        color:'white',
        alignSelf:'center',
        marginTop:hp('5%'),
        marginBottom:hp('5%')
    },
    
})
export default YourLocation;