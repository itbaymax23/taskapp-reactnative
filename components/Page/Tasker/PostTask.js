import React from 'react';
import {View,Text,TouchableOpacity,StyleSheet} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Entypo} from '@expo/vector-icons';
import {HomeLayout} from '../../Layout';
import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';

class PostTask extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
           
        }
    }

    gettitle = () => {
        return (
            <View style={{...Style.card,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{position:'absolute'}} onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>Post a Task</Text>
                </View>
            </View>
        )
    }


    render()
    {
        return (
            <HomeLayout placeholder="Find Tasker" title={this.gettitle()}>
                <View style={{...Style.card,alignItems:'center',marginBottom:5}}>
                    <Text style={Style.title1}>Task Address</Text>
                </View>
                <View style={{flexDirection:'row',marginBottom:3}}>
                    <View style={{...Style.card,flex:1}}>
                        <Text style={Style.defaulttext}>San Francisko, CA, USA</Text>
                    </View>
                    <TouchableOpacity style={{...style.iconcontainer,backgroundColor:Color.activecolor}}>
                        <Entypo name="chevron-thin-right" style={{...style.lefticon,color:"white"}}></Entypo>
                    </TouchableOpacity>
                </View>
                <View style={{...Style.card,alignItems:'center',marginBottom:5}}>
                    <Text style={Style.title1}>When</Text>
                </View>
                <View style={{flexDirection:'row',marginBottom:3}}>
                    <View style={{...Style.card,flex:1}}>
                        <Text style={Style.defaulttext}>Thu,Sep 19, 2019</Text>
                    </View>
                    <TouchableOpacity style={style.iconcontainer}>
                        <Entypo name="chevron-thin-right" style={{...style.lefticon,color:"#808E99"}}></Entypo>
                    </TouchableOpacity>
                </View>
                <View style={{...Style.card,alignItems:'center',marginBottom:5}}>
                    <Text style={Style.title1}>Task Size</Text>
                </View>
                <View style={{flexDirection:'row',marginBottom:3}}>
                    <View style={{...Style.card,flex:1}}>
                        <Text style={Style.defaulttext}>Medium - Est. 2 - 3 hrs</Text>
                    </View>
                    <TouchableOpacity style={style.iconcontainer}>
                        <Entypo name="chevron-thin-right" style={{...style.lefticon,color:"#808E99"}}></Entypo>
                    </TouchableOpacity>
                </View>
                <View style={{...Style.card,alignItems:'center',marginBottom:5}}>
                    <Text style={Style.title1}>Task Detals</Text>
                </View>
                <View style={{flexDirection:'row',marginBottom:3}}>
                    <View style={{...Style.card,flex:1}}>
                        <Text style={Style.defaulttext}>3 walls 2 sealings</Text>
                    </View>
                    <TouchableOpacity style={style.iconcontainer}>
                        <Entypo name="chevron-thin-right" style={{...style.lefticon,color:"#808E99"}}></Entypo>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:hp('3%'),alignItems:'center'}}>
                    <Button color={Color.deactivecolor} style={Style.buttontext1}>Next</Button>
                </View>
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    lefticon:{
        fontSize:hp('3.5%')
    },
    iconcontainer:{
        paddingLeft:wp('5%'),
        paddingRight:wp('5%'),
        justifyContent:'center',
        backgroundColor:'white',
        marginBottom:2
    }
})
export default PostTask;