import React from 'react';
import {View,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {HomeLayout} from '../../Layout';
import Style from '../../style';
import {Button} from '../../Element';
import * as Color from '../../colors';

class Home extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <HomeLayout placeholder="Find Tasker">
                <View style={Style.card}>
                    <Text style={{...Style.jobtitle,textAlign:'center'}}>GET READY TO WRITE YOUR TASK POST</Text>
                </View>
                <View style={Style.card}>
                    <Text style={{...Style.description,textAlign:'center'}}>You can create a new task post.</Text>
                </View>
                <View style={Style.card}>
                    <Text style={{...Style.description,textAlign:'center'}}>
                        Details about your project: Include the timeline, {'\n'} budget, and key deliverables.
                    </Text>
                </View>
                <View style={{...Style.card,paddingTop:hp('6.5%'),paddingBottom:hp('6.5%')}}>
                    <Text style={{...Style.description,textAlign:'center'}}>
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    </Text>
                </View>
                <View style={{flex:1,alignItems:'center',marginTop:hp('10%')}}>
                    <Button onPress={()=>this.props.navigation.navigate("PostNew")} color={Color.deactivecolor} style={Style.buttontext1}>Post a new Task</Button>
                </View>
            </HomeLayout>
        )
    }
}

export default Home;