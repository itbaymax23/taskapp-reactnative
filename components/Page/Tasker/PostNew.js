import React from 'react';
import {View,Text,TouchableOpacity} from 'react-native';
import {Entypo} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {HomeLayout} from '../../Layout';
import Style from '../../style';
import {Button} from '../../Element';
import * as Color from '../../colors';


class PostNew extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            select:"",
            data:[
                {title:"Step1",desc:"Task Title"},
                {title:"Step2",desc:"Task  Description"},
                {title:"Step3",desc:"To - Do List"},
                {title:"Step4",desc:"Address"},
                {title:"Step5",desc:"Term of the Task"},
                {title:"Step6",desc:"Task size"},
                {title:"Step7",desc:"Task Details"},
                {title:"Step8",desc:"Budget"},
                {title:"Step9",desc:"Review and Post"}
            ]
        }
    }

    select = (item) =>{
        this.setState({
            select:item
        })
    }
    render()
    {
        return (
            <HomeLayout placeholder="Find Tasker">
                <View style={Style.card}>
                    <Text style={{...Style.jobtitle,textAlign:'center'}}>How to Post a Task</Text>
                </View>
                <View style={Style.card}>
                    <Text style={{...Style.defaulttext,textAlign:'center'}}>Here’s an overview of the process from {'\n'} start to finish.</Text>
                </View>
                {
                    this.state.data.map((row,index)=>{
                        return (
                            <TouchableOpacity style={{flexDirection:'row',marginBottom:2}} key={index} onPress={()=>this.select(row.title)}>
                                <View style={{...Style.carditem,marginRight:2}}>
                                    <Text style={Style.defaulttext}>{row.title}</Text>
                                </View>
                                <View style={{...Style.carditem,marginRight:2,flex:1}}>
                                    <Text style={Style.defaulttext}>{row.desc}</Text>
                                </View>
                                <TouchableOpacity style={{...Style.carditem,marginRight:2,justifyContent:'center',backgroundColor:this.state.select == row.title?Color.activecolor:"white"}} onPress={()=>this.props.navigation.navigate("Help")}>
                                    <Entypo name="chevron-thin-right" style={{...Style.defaulticon,color:this.state.select == row.title?"white":"#808E99"}}></Entypo>
                                </TouchableOpacity>
                            </TouchableOpacity>
                        )
                    })
                }
                <View style={{marginTop:hp('3%'),marginBottom:hp('2%'),alignItems:'center'}}>
                    <Button color={Color.deactivecolor} style={Style.buttontext1} onPress={()=>this.props.navigation.navigate("TaskPost")}>Post a Task</Button>
                </View>
            </HomeLayout>
        )
    }
}

export default PostNew;