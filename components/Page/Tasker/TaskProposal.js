import React from 'react';
import {View,Text,TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Entypo} from '@expo/vector-icons';
import {HomeLayout} from '../../Layout';
import Style from '../../style';
import * as Color from '../../colors';
import {TextArea, Button} from '../../Element';

class TaskProposal extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            data:{
                title:"",
                description:""
            },
            success:{

            }
        }
    }

    gettitle = () => {
        return (
            <View style={{...Style.card,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{position:'absolute'}} onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>Post a Task</Text>
                </View>
            </View>
        )
    }

    next = () => {
        if(Object.keys(this.state.success).length == 2)
        {
            this.props.navigation.navigate("Todo");
        }
    }

    

    render()
    {
        return (
            <HomeLayout placeholder="Find Tasker" title={this.gettitle()}>
                <View style={Style.card}>
                    <Text style={{...Style.jobtitle,textAlign:'center'}}>Title</Text>
                </View>
                <View style={Style.card}>
                    <TextArea placeholder="Write your title" handlechange={(text)=>this.handlechange("title",text)} success={this.state.success.title}></TextArea>
                </View>
                <View style={Style.card}>
                    <Text style={{...Style.jobtitle,textAlign:'center'}}>Description</Text>
                </View>
                <View style={Style.card}>
                    <TextArea numberofline={5} placeholder="Your description" handlechange={(text)=>this.handlechange("description",text)} success={this.state.success.description}></TextArea>
                </View>
                <View style={{flex:1,alignItems:'center',marginTop:hp('5%')}}>
                    <Button color={Object.keys(this.state.success).length == 2?Color.activecolor:Color.deactivecolor} style={Style.buttontext1} onPress={this.next}>Next</Button>
                </View>
            </HomeLayout>
        )
    }
}

export default TaskProposal;