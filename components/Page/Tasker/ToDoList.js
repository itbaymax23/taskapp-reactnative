import React from 'react';
import {View,StyleSheet,TouchableOpacity,Text, Alert} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Entypo} from '@expo/vector-icons';


import {HomeLayout} from '../../Layout';
import Style from '../../style';
import * as Color from '../../colors';
import {Button} from '../../Element';

class ToDoList extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            todo:["Help Moving","Furniture Assembly","Mounting","Home Repaires","Cleaning","Heavy Lifting",
            "Delivery","Yard Work","Personal Assistant","Packing & Unpacking","Painting","Deep Clean","Organization",
            "Event Staffing","Run Errands","Wait in Line","Carpentry"],
            selected:[]
        }
    }

    componentDidMount(props)
    {
        let selectedtodos = [];
        
        if(this.props.navigation.state.params && this.props.navigation.state.params.todos)
        {
            selectedtodos = this.props.navigation.state.params.todos;
        }

        this.setState({
            selected:selectedtodos
        })
    }

    gettitle = () => {
        return (
            <View style={{...Style.card,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{position:'absolute'}} onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>To - Do List</Text>
                </View>
            </View>
        )
    }

    accept = () => {
        if(this.state.selected.length == 0)
        {
            Alert.alert("Error","Please select at least one category");
        }
        else
        {
            this.props.navigation.goBack();
            if(this.props.navigation.state.params && this.props.navigation.state.params.update)
            {
                this.props.navigation.state.params.update(this.state.selected);    
            }
            
        }
    }

    select = (row) => {
        let selectedtodo = this.state.selected;
        if(selectedtodo.indexOf(row) > -1)
        {
            selectedtodo.splice(selectedtodo.indexOf(row),1);
        }
        else
        {
            selectedtodo.push(row);
        }

        this.setState({
            selected:selectedtodo
        })
    }

    render()
    {
        
        return (
            <HomeLayout title={this.gettitle()} placeholder="Find Tasker">
                <View style={{flexDirection:'row',justifyContent:'center',padding:17,flexWrap:'wrap'}}>
                    {
                        this.state.todo.map((row,index)=>{
                            return (
                                <TouchableOpacity onPress={()=>this.select(row)} key={index} style={{...style.todoitem,backgroundColor:this.state.selected.indexOf(row)>-1?Color.activecolor:'white'}}>
                                    <Text style={{...Style.defaulttext,color:this.state.selected.indexOf(row)>-1?'white':'#384D5F'}}>{row}</Text>
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
                <View style={{alignItems:'center',marginTop:hp('6.5%')}}>
                    <Button color={Color.deactivecolor} style={Style.buttontext1} onPress={this.accept}>Accept</Button>      
                </View>
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    todoitem:{
        paddingLeft:wp('4.6%'),
        paddingRight:wp('4.6%'),
        paddingTop:hp('1.25%'),
        paddingBottom:hp('1.25%'),
        marginBottom:hp('1.25%'),
        marginRight:wp('3%'),
        borderRadius:4
    }
})

export default ToDoList;