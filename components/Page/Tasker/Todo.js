import React from 'react';
import {View,TouchableOpacity,Text,Image,StyleSheet} from 'react-native';
import {Entypo,AntDesign} from '@expo/vector-icons';
import {heightPercentageToDP as hp,widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {HomeLayout} from '../../Layout';
import Style from '../../style';
import * as Color from '../../colors';
import {Button,TodoItem} from '../../Element';

class ToDo extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            todo:[]
        }
    }

    gettitle = () => {
        return (
            <View style={{...Style.card,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{position:'absolute'}} onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>Post a Task</Text>
                </View>
            </View>
        )
    }

    update = (todos) => {
        this.setState({
            todo:todos
        })
    }

    todolist = () => {
        this.props.navigation.navigate("TodoList",{todos:this.state.todo,update:this.update});
    }

    remove = (row) => {
        let todo = this.state.todo;
        todo.splice(todo.indexOf(row),1);
        this.setState({
            todo:todo
        })
    }

    accept = () => {
        this.props.navigation.navigate('Step1');
    }


    render()
    {
        return (
            <HomeLayout title={this.gettitle()} placeholder="Find Tasker">
                <View style={{flex:1,minHeight:hp('25%')}}>
                    {
                        this.state.todo.length > 0 && (
                            <View style={{flexDirection:'row',paddingLeft:wp('5.5%'),paddingRight:wp('5.5%'),flexWrap:'wrap'}}>
                                {
                                    this.state.todo.map((row,index)=>{
                                        return (
                                            <TodoItem key={index} todo={row} onPress={()=>this.remove(row)}></TodoItem>
                                        )
                                    })
                                }
                            </View>
                        )
                    }
                    
                    <View style={{flexDirection:'row',marginTop:hp('1%')}}>
                        <View style={{flex:1,...Style.card}}>
                            <Text style={Style.defaulttext}>Add Things here you need to get done</Text>
                        </View>
                        <TouchableOpacity style={style.plusiconcontainer} onPress={this.todolist}>
                            <AntDesign name="plus" style={style.plusicon}></AntDesign>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flex:2}}>
                    <View style={{paddingLeft:wp('8%'),paddingRight:wp('8%'),alignItems:'center'}}>
                        <Image source={require('../../../assets/icons/document.png')}></Image>
                        <Text style={{marginTop:hp('6%'),...Style.title2}}>Add Things to your to - do list</Text>
                        <Text style={{...Style.description1,textAlign:'center',marginTop:hp('2.2%')}}>Jot down reminders of things you need to get done. We’ll help you match these itemsto the {'\n'} right category</Text>
                    </View>
                    {
                        this.state.todo.length > 0 && (
                            <View style={{marginTop:hp('3%'),alignItems:'center'}}>
                                <Button color={Color.deactivecolor} style={Style.buttontext1} onPress={this.accept}>Accept</Button>
                            </View>
                        )
                    }
                    
                </View>
            </HomeLayout>
        )
    }
}

const style = StyleSheet.create({
    plusiconcontainer:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:Color.activecolor,
        borderRadius:2,
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        marginLeft:wp('1%')
    },
    plusicon:{
        fontSize:hp('3.5%'),
        color:'white'
    },
    image:{
        width:hp('10%'),
        height:hp('12%')        
    }
})
export default ToDo;