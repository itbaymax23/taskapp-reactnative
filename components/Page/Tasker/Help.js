import React from 'react';
import {View,Text,TouchableOpacity} from 'react-native';
import {Entypo} from '@expo/vector-icons';
import {HomeLayout} from '../../Layout';
import Style from '../../style';

class Help extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    gettitle = () => {
        return (
            <View style={{...Style.card,flexDirection:'row',alignItems:'center'}}>
                <TouchableOpacity style={{position:'absolute'}} onPress={()=>this.props.navigation.goBack()}>
                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                </TouchableOpacity>
                <View style={{flex:1,alignItems:'center'}}>
                    <Text style={Style.title1}>Budget</Text>
                </View>
            </View>
        )
    }

    render()
    {
        return (
            <HomeLayout placeholder="Find Tasker" title={this.gettitle()}>
                <View style={Style.card}>
                    <Text style={{...Style.description,textAlign:'center'}}>
                        A fixed-price project has a set price that’s {'\n'}
                        either paid all at once or by {'\n'}
                        milestone—predetermined deadlines that {'\n'}
                        break your tasks into smaller pieces of work. {'\n'}
                        The funds are deposited into escrow at the {'\n'}
                        beginning of the project and/or milestone, then {'\n'}
                        released as you approve the work.
                    </Text>
                </View>
                <View style={Style.card}>
                    <Text style={{...Style.description,textAlign:'center'}}>An hourly project is paid by the hour, with the tasker tracking the time they spend working on your task. Hours are then billed on a weekly basis; as the client, you can set a cap on the number of hours billed every week.</Text>
                </View>
            </HomeLayout>
        )
    }
}

export default Help;