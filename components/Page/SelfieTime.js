import React from 'react';
import {Image,View,StyleSheet,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {RegisterLayout} from '../Layout';
import {Button} from '../Element';
import Style from '../style';
class SelfieTime extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onPress = () => {
        this.props.navigation.navigate("YourLocation");
    }
    render()
    {
        return (
            <RegisterLayout {...this.props} title={"Selfie Time"}>
                <Image style={style.icon} source={require('../../assets/icons/self_time.png')}></Image>
                <Text style={style.description}>
                    We just use your photo to double- {'\n'}
                    check it with your documents.
                </Text>
                <View>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Take Picture</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        width:hp('18%'),
        height:hp('22%'),
        marginTop:hp('10%'),
        marginBottom:hp('10%'),
        alignSelf:'center'
    },
    description:{
        fontWeight:'bold',
        textAlign:'center',
        lineHeight:hp('4%'),
        marginBottom:hp('10%'),
        color:'white',
        fontSize:hp('2.5%'),
        fontFamily:"Arial"
    }
})
export default SelfieTime;