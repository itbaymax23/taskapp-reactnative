import {View,Image,StyleSheet} from 'react-native';
import React from 'react';
import {AuthLayout} from '../Layout';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Button} from '../Element';
import Style from '../style';

class FirstPage extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    buttonpressed = () =>{
        this.props.navigation.navigate("SelectUserType");
    }   

    onPressedLogin = () => {
        this.props.navigation.navigate('Tasker');
    }

    render()
    {
        return(
            <AuthLayout>
                <View style={style.container}>
                    <Image source={require('../../assets/icons/logo.png')} style={style.logo}></Image>
                    <View style={{marginTop:hp('15%')}}>
                        <Button style={Style.buttontext_auth} onPress={this.onPressedLogin}>Login</Button>
                    </View>
                    <View style={{marginTop:hp('2.3%')}}>
                        <Button style={Style.buttontext_auth} onPress={this.buttonpressed}>Register</Button>
                    </View>
                </View>
            </AuthLayout>
        )
    }
}

const style = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%')
    },
    logo:{
        height:hp('18%'),
        width:hp('18%')
    }
})

export default FirstPage;