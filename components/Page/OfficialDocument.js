import React from 'react';
import {Image,TouchableOpacity,StyleSheet,Text, View} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {RegisterLayout} from '../Layout';
import {Button} from '../Element';
import Styles from '../style';
class OfficialDocument extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            selected:""
        }
    }

    selecttype = (type) => {
        this.setState({
            selected:type
        })
    }

    getimage = () => {
        let type = this.state.selected;
        switch(type)
        {
            case 'passport':
                return require('../../assets/icons/passport.png');
            case 'driver':
                return require('../../assets/icons/driver_license.png');
            case 'national':
                return require('../../assets/icons/nationalid.png');
            default:
                return require('../../assets/icons/document_type.png');
        }
    }

    next = () => {
        this.props.navigation.navigate('VerifyIdentity');
    }
    render()
    {
        return (
            <RegisterLayout  {...this.props} title={"Select your official \n Document"}>
                <View style={style.imagecontainer}>
                    <Image source={this.getimage()}></Image>
                </View>
                <Text style={style.title}>Select Document type</Text>
                <View style={style.typecontainer}>
                    <TouchableOpacity onPress={()=>this.selecttype("passport")} style={{backgroundColor:this.state.selected != 'passport'?'transparent':"white",...style.left}}>
                        <Text style={{color:this.state.selected == 'passport'?'black':'white',...style.typetext}}>Passport</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selecttype("driver")} style={{backgroundColor:this.state.selected != 'driver'?'transparent':"white",...style.middle}}>
                        <Text style={{color:this.state.selected == 'driver'?'black':'white',...style.typetext}}>Driver's License</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.selecttype("national")} style={{backgroundColor:this.state.selected != 'national'?'transparent':"white",...style.right}}>
                        <Text style={{color:this.state.selected == 'national'?'black':'white',...style.typetext}}>National ID</Text>
                    </TouchableOpacity>
                </View>
                <View style={{marginTop:hp('20%')}}>
                    <Button style={Styles.buttontext_auth} onPress={this.next}>Next</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    imagecontainer:{
        height:hp('23%'),
        marginTop:hp('5%'),
        marginBottom:hp('7%'),
        justifyContent:'center',
        alignItems:'center'
    },
    title:{
        fontSize:hp('2.3%'),
        fontWeight:'500',
        color:'white',
        textAlign:'center',
        marginBottom:hp('4%'),
        fontFamily:"Arial"
    },
    typecontainer:{
        flex:1,
        height:hp('6.3%'),
        borderRadius:hp('3.2%'),
        borderColor:'white',
        borderWidth:2,
        flexDirection:'row'
    },
    left:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        borderTopLeftRadius:hp('3.2%'),
        borderBottomLeftRadius:hp('3.2%')
    },
    right:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        borderTopRightRadius:hp('3.2%'),
        borderBottomRightRadius:hp('3.2%')
    },
    middle:{
        flex:1,
        borderLeftColor:'white',
        borderLeftWidth:2,
        borderRightColor:'white',
        borderRightWidth:2,
        justifyContent:'center',
        alignItems:'center'
    },
    typetext:{
        fontWeight:'bold',
        fontSize:hp('1.7%'),
        fontFamily:"Arial"
    }
})
export default OfficialDocument;