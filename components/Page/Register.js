import React from 'react';
import {Text,View,Image} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

import {Button,Input} from '../Element';
import {RegisterLayout} from '../Layout';
import Style from '../style';
import * as Utils from '../Tools/Utils';

class Register extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            fill:0,
            userdata:{},
            success:{},
            error:{}
        }
    }

    handleChange = (name,value) => {
        let success = this.state.success;
        let userdata = this.state.userdata;
        if(name == 'email')
        {
            if(Utils.validateEmail(value))
            {
                success.email = true;
            }
            else
            {
               success.email = false;
            }
        }
        else if(name == 'password')
        {
            if(value)
            {
                success.password = true;
                if(userdata.confirmpassword == value)
                {
                    success.confirmpassword = true;
                }
                else
                {
                    success.confirmpassword = false;
                }
            }
            else
            {
                success.password = false;
                success.confirmpassword = false;
            }
        }
        else if(name == 'confirmpassword')
        {
            if(value && value == userdata.password)
            {
                success.confirmpassword = true;
            }
            else
            {
                success.confirmpassword = false;
            }
        }
   
        userdata[name] = value;
   
        console.log(success);
        this.setState({
            userdata:userdata,
            success:success,
            error:{}
        })
    }

    validate = () => {
		let error = {};
		let userdata = this.state.userdata;
        let success = this.state.success;
        let enable = true;
		if(!userdata.email)
		{
			error.email = "This field is required";
            success.email = false;
            enable = false;
		}
		else if(!Utils.validateEmail(userdata.email))
		{
			error.email = "The email is not valid";
            success.email = false;
            enable = false;
		}
		else
		{
            error.email = false;
		}

		if(!userdata.password)
		{
			error.password = "This field is required";
            success.password = false;
            enable = false;
		}
		else
		{
			error.password = false;
            success.password = true;
		}

		if(!userdata.confirmpassword)
		{
			error.confirmpassword = "This field is required";
            success.confirmpassword = false;
            enable = false;
		}
		else if(userdata.confirmpassword != userdata.password)
		{
			error.confirmpassword = "Password have to be same as Confirm Password";
            success.confirmpassword = false;
            enable = false;
		}
		else
		{
			error.confirmpassword = false;
		}

		this.setState({
			error:error,
			success:success
        })	
        
        return enable;
	}

    next = () => {
        if(this.validate())
        {
            this.props.navigation.navigate('RegisterProfile');
        }
    }

    render()
    {
        return (
            <RegisterLayout back={true} {...this.props} title="Get Your Account">                
                <AnimatedCircularProgress
                 width={5} 
                 fill={this.state.fill} 
                 backgroundColor="#08223919" 
                 tintColor='white' 
                 size={hp('9%')} 
                 style={{marginTop:hp('9%'),alignSelf:'center'}}                 
                 >
                     {()=>(
                        <Text style={Style.progress_txt}>{this.state.fill} %</Text>
                     )}
                </AnimatedCircularProgress>
                <View style={{marginTop:hp('15%')}}>
                    <Input label="Email address" icon="ios-mail" success={this.state.success.email} error={this.state.error.email} bordertop={true} onChange={(value)=>this.handleChange('email',value)}></Input>
                    <Input type="password" label="Password" icon="ios-lock" success={this.state.success.password} error={this.state.error.password} onChange={(value)=>this.handleChange('password',value)}></Input>
                    <Input type="password" label="Confirm Password" icon="ios-lock" success={this.state.success.confirmpassword} error={this.state.error.confirmpassword} onChange={(value)=>this.handleChange('confirmpassword',value)}></Input>
                </View>
                <View style={{flex:1,marginTop:hp('6%')}}>
                    <Button style={Style.buttontext_auth} onPress={this.next}>Next</Button>
                    <View style={{marginTop:hp('2%')}}>
                        <Button style={Style.buttontext_auth} icon={<Image source={require('../../assets/icons/facebookicon.png')} style={Style.icon}></Image>}>Sign up via Facebook</Button>
                    </View>
                    <View style={{marginTop:hp('2%')}}>
                        <Button style={Style.buttontext_auth} icon={<Image source={require('../../assets/icons/googleicon.png')} style={Style.icon}></Image>}>Sign up via Google</Button>
                    </View>
                </View>
            </RegisterLayout>
        )
    }
}

export default Register;