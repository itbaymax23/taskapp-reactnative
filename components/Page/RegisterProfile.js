import React from 'react';
import {View,StyleSheet,TouchableOpacity,Text} from 'react-native';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Entypo} from '@expo/vector-icons';

import {RegisterLayout} from '../Layout';
import {Button,Input} from '../Element';
import Style from '../style';


class RegisterProfile extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            fill:100,
            userdata:{},
            success:{},
            error:{}
        }
    }

    handleChange = (name,value) => {
        let data = this.state.userdata;
        data[name] = value;
        
        this.validate(data);

        this.setState({
            userdata:data
        })
    }

    validate = (data) => {
        let success = {};
        if(data.firstname)
        {
            success.firstname = true;
        }

        if(data.lastname)
        {
            success.lastname = true;
        }

        this.setState({
            success:success
        })
    }

    next = () =>{
        this.props.navigation.navigate('Verify');
    }

    render()
    {
        return (
            <RegisterLayout back={true} {...this.props} title="More about You">
                <AnimatedCircularProgress
                 width={5} 
                 fill={this.state.fill} 
                 backgroundColor="#08223919" 
                 tintColor='white' 
                 size={hp('9%')} 
                 style={{marginTop:hp('9%'),alignSelf:'center'}}                 
                 >
                     {()=>(
                        <Text style={Style.progress_txt}>{this.state.fill} %</Text>
                     )}
                </AnimatedCircularProgress>
                <View style={style.body}>
                    <View style={style.profilecontainer}>
                        <View style={style.profile}>
                            <Entypo name="user" style={style.profileicon}></Entypo>
                        </View>
                        <TouchableOpacity style={style.photo}>
                            <Entypo name="camera" style={style.photoicon}></Entypo>
                        </TouchableOpacity>
                    </View>
                    <View style={style.inputcontainer}>
                        <Input label="First Name" success={this.state.success.firstname} error={this.state.error.firstname} bordertop={true} onChange={(value)=>this.handleChange('firstname',value)}></Input>
                        <Input label="Last Name" success={this.state.success.lastname} error={this.state.error.lastname} onChange={(value)=>this.handleChange('lastname',value)}></Input>
                    </View>
                    <View style={style.buttoncontainer}>
                        <Button style={Style.buttontext_auth} onPress={this.next}>Next</Button>
                    </View>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    body:{
        flex:1
    },
    inputcontainer:{
        marginTop:hp('5%')
    },
    profile:{
        width:hp('14%'),
        height:hp('14%'),
        borderRadius:hp('7%'),
        backgroundColor:'#FFFFFF80',
        alignItems:'center',
        overflow:'hidden'
    },
    profilecontainer:{
        width:hp('14%'),
        height:hp('14%'),
        alignSelf:'center',
        position:'relative',
        marginTop:hp('5%')
    },
    profileicon:{
        fontSize:hp('14%'),
        alignSelf:'center'
    },
    photo:{
        width:hp('4.5%'),
        height:hp('4.5%'),
        backgroundColor:'white',
        zIndex:100,
        position:'absolute',
        bottom:0,
        right:0,
        borderRadius:hp('2%'),
        justifyContent:'center',
        textAlign:'center'
    },
    photoicon:{
       fontSize:hp('2.5%'),
       marginLeft:hp('1%'),
       color:'#4EA9B6'
    },
    buttoncontainer:{
        flex:1,
        marginTop:hp('20%')
    }
})
export default RegisterProfile;