import React from 'react';
import {createAppContainer} from 'react-navigation';
import {Image,Text,StyleSheet} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {Ionicons,FontAwesome} from '@expo/vector-icons';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';

import * as Color from '../colors';
import {Home,PostNew,Help,TaskPost,Todo,TodoList, Step1} from './Tasker';

const HomeStack = createStackNavigator({
    Home:{
        screen:Home   
    },
    PostNew:{
        screen:PostNew
    },
    Help:{
        screen:Help
    },
    TaskPost:{
        screen:TaskPost
    },
    Todo:{
        screen:Todo
    },
    TodoList:{
        screen:TodoList
    },
    Step1:{
        screen:Step1
    }
},{
    defaultNavigationOptions:{
        header:()=>null
    }
})

const style = StyleSheet.create({
    icon:{
        fontSize:hp('3%')
    },
    iconimage:{
        width:hp('3%'),
        height:hp('3%')
    },
    icontext:{
        fontSize:hp('1.8%'),
        fontWeight:'500'
    }
})

const App = createBottomTabNavigator({
    Home:{
        screen:HomeStack,
        navigationOptions:{
            tabBarLabel:"Home"
        }
    },
    Tasks:{
        screen:Home,
        navigationOptions:{
            tabBarLabel:"Tasks"
        }
    },
    MyTaskers:{
        screen:Home,
        navigationOptions:{
            tabBarLabel:"My Taskers"
        }
    },
    Profile:{
        screen:Home,
        navigationOptions:{
            tabBarLabel:"Profile"
        }
    }
},{
    defaultNavigationOptions:({navigation})=>({
            tabBarIcon:({focused,horizontal,tintColor}) => {
                const {routeName} = navigation.state;
                let iconname = "";
                let Iconelement = Ionicons;
                switch(routeName)
                {
                    case 'Home':
                        iconname = "md-home";
                        Iconelement = Ionicons;
                        break;
                    case 'MyTaskers':
                        iconname = "users";
                        Iconelement = FontAwesome;
                        break;
                    case 'Profile':
                        iconname = "user";
                        Iconelement = FontAwesome;
                        break;
                }

                if(routeName == 'Tasks')
                {
                    return <Image source={require('../../assets/icons/job.png')} style={style.iconimage}></Image>
                }
                else
                {
                    return <Iconelement name={iconname} style={{color:focused?Color.activecolor:Color.icondeactive,...style.icon}}></Iconelement>
                }
            }
        }
    )
});

export default createAppContainer(App);