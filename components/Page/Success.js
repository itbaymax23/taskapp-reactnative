import React from 'react';
import {Image,View,StyleSheet,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Ionicons} from '@expo/vector-icons';

import {RegisterLayout} from '../Layout';
import {Button} from '../Element';
import Style from '../style';
class Success extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onPress = () => {
        this.props.navigation.navigate("Main");
    }
    render()
    {
        return (
            <RegisterLayout title={"Congratulations"}>
                <Ionicons style={style.icon} name="ios-checkmark-circle"></Ionicons>
                <Text style={style.description}>
                    Your identity has been verified {'\n'}
                    successfully.
                </Text>
                <View>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Go to Home page</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        fontSize:hp('13%'),
        marginTop:hp('4%'),
        marginBottom:hp('10%'),
        alignSelf:'center',
        color:'white'
    },
    description:{
        fontWeight:'bold',
        textAlign:'center',
        lineHeight:hp('4%'),
        marginBottom:hp('10%'),
        color:'white',
        fontSize:hp('2.2%'),
        fontFamily:"Arial"
    }
})
export default Success;