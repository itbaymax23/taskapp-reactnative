import React from 'react';
import {Image,View,StyleSheet,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {RegisterLayout} from '../Layout';
import {Button} from '../Element';
import Style from '../style';
class Verify extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onPress = () => {
        this.props.navigation.navigate("OfficialDocument");
    }
    render()
    {
        return (
            <RegisterLayout  {...this.props} title={"Verify Your Identity"}>
                <Image style={style.icon} source={require('../../assets/icons/verify.png')}></Image>
                <Text style={style.title}>Tow Quick Steps</Text>
                <Text style={style.description}>
                    We take tow simple steps to protect against {'\n'}
                    financical crime. It should only take a minute or {'\n'}
                    tow. Here’s what you need:
                </Text>
                <Text style={style.description}>
                    Your Passport, Driver’s Licence or National {'\n'}
                    ID to Hand {'\n'}
                    Record a 3 second selfie video {'\n'}
                </Text>
                <View>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Verify Now</Button>
                </View>
                <View style={{marginTop:hp('2.6%'),marginBottom:hp('5%')}}>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Skip action</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        width:hp('13%'),
        height:hp('16%'),
        marginTop:hp('4%'),
        marginBottom:hp('4%'),
        alignSelf:'center'
    },
    title:{
        fontSize:hp('4%'),
        fontWeight:'bold',
        textAlign:'center',
        color:'white',
        marginBottom:hp('3%'),
        fontFamily:"Arial"
    },
    description:{
        fontWeight:'500',
        textAlign:'center',
        lineHeight:hp('4%'),
        marginBottom:hp('5%'),
        color:'white',
        fontSize:hp('2.2%'),
        fontFamily:"Arial"
    }
})
export default Verify;