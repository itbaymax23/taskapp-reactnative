import React from 'react';
import {Image,View,StyleSheet,Text} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CheckBox from 'react-native-check-box';

import {RegisterLayout} from '../Layout';
import {Button} from '../Element';
import Style from '../style';
import {Ionicons,MaterialIcons} from '@expo/vector-icons';
class VerifyIdentity extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            checked:false
        }
    }

    setchecked = () => {
        let checked = !this.state.checked;
        this.setState({
            checked:checked
        })
    }

    onPress = () => {
        this.props.navigation.navigate("SelfieTime");
    }
    render()
    {
        return (
            <RegisterLayout  {...this.props} title={"Verify Your Identity"}>
                <Image style={style.icon} source={require('../../assets/icons/verify_identity.png')}></Image>
                <Text style={style.description}>
                    A Picture of your chosen document, {'\n'}
                    a selfie, and some good lightning
                </Text>
                <View style={{paddingLeft:wp('5%'),paddingRight:wp('5%')}}>
                    <CheckBox 
                    style={{flex:1,padding:10}} 
                    onClick={this.setchecked} 
                    isChecked={this.state.checked} 
                    rightText={"I agree and confirm to have read and accepted \n the T&Cs and data policy!"}
                    rightTextStyle={style.checkbox}
                    checkBoxColor="white"
                    checkedImage={<Ionicons name="ios-checkbox-outline" style={style.checkedicon}></Ionicons>}
                    uncheckedImage={<MaterialIcons name="check-box-outline-blank" style={style.checkedicon}></MaterialIcons>}
                    ></CheckBox>
                </View>
                <View style={{marginTop:hp('10%')}}>
                    <Button style={Style.buttontext_auth} onPress={this.onPress}>Next</Button>
                </View>
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        width:hp('22%'),
        height:hp('27.5%'),
        marginTop:hp('4%'),
        marginBottom:hp('6%'),
        alignSelf:'center'
    },
    description:{
        fontWeight:'bold',
        textAlign:'center',
        lineHeight:hp('4%'),
        marginBottom:hp('5%'),
        color:'white',
        fontSize:hp('2.2%'),
        fontFamily:"Arial"
    },
    checkbox:{
        fontSize:hp('1.8%'),
        fontWeight:'500',
        color:'white',
        lineHeight:hp('2.5%'),
        fontFamily:"Arial"
    },
    checkedicon:{
        fontSize:hp('4%'),
        color:'white',
        fontWeight:'bold'
    }
})
export default VerifyIdentity;