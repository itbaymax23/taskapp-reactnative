import React from 'react';
import {TouchableOpacity,View,StyleSheet,Text,Animated, Easing} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {RegisterLayout} from '../Layout';
class Confirm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.rotationViewHolder = new Animated.Value(0);
    }

    componentDidMount()
    {
        this.startAnimation();
    }

    startAnimation = () => {
        this.rotationViewHolder.setValue(0);
        Animated.timing(this.rotationViewHolder,{
            toValue:1,
            duration:3000,
            easing:Easing.linear
        }).start(() => this.startAnimation());
    }

    onPress = () => {
        this.props.navigation.navigate("Success");
    }
    render()
    {
        const RotateData = this.rotationViewHolder.interpolate({
            inputRange:[0,1],
            outputRange:['0deg','360deg']
        });
        
        return (
            <RegisterLayout title={"Upload complete"}>
                <Animated.Image style={{transform:[{rotate:RotateData}],...style.icon}} source={require('../../assets/icons/loading.png')}></Animated.Image>
                <Text style={style.description}>
                    We are checking your documents {'\n'}
                    It will take up to 15 mins {'\n'}
                    the process to be completed.
                </Text>
                <TouchableOpacity onPress={this.onPress}>
                    <Text style={style.description}>You can continue from home page.</Text>    
                </TouchableOpacity>
                
            </RegisterLayout>
        )
    }
}

const style = StyleSheet.create({
    icon:{
        marginTop:hp('8%'),
        marginBottom:hp('8%'),
        alignSelf:'center'
    },
    description:{
        fontWeight:'500',
        textAlign:'center',
        lineHeight:hp('4%'),
        marginBottom:hp('8%'),
        color:'white',
        fontSize:hp('2.2%'),
        fontFamily:"Arial"
    }
})
export default Confirm;