import React from 'react';
import {Ionicons} from '@expo/vector-icons';
import {View,StyleSheet,TouchableOpacity,Text,Image} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

import * as Color from '../colors';
import Style from '../style';

class JobLayout extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    getreview = (review) => {
        let reviewelement = [];

        for(let item = 0;item < review;item++)
        {
            reviewelement.push(<Ionicons key={item} name="ios-star" style={{color:Color.activestar,...Style.star}}></Ionicons>);
        }

        for(let item = review;item<5;item++)
        {
            reviewelement.push(<Ionicons key={item} name="ios-star" style={{color:Color.deactivecolor,...Style.star}}></Ionicons>);
        }

        return reviewelement;
    }

    render()
    {
        return (
            <TouchableOpacity onPress={this.props.onPressed} style={{backgroundColor:this.props.data.recent?Color.recentlayout:'white',...style.joblayout}}>
                <View style={{flex:6}}>
                    <View style={style.review}>
                        <View style={{flexDirection:'row'}}>
                            {this.getreview(this.props.data.review)}
                        </View>
                        <Text style={{marginLeft:wp('3.5%'),color:Color.reviewcolor,...Style.jobtitle}}>({this.props.data.reviewcount})</Text>
                    </View>
                    <Text style={Style.jobtitle}>{this.props.data.title}</Text>
                    <View style={style.description}>
                        <Text style={Style.jobdescription}>{this.props.data.description}</Text>
                    </View>
                </View>
                <View style={{flex:3}}>
                    <View style={{marginLeft:'auto',flexDirection:'row'}}>
                        <View style={style.item}>
                            <Image source={this.props.data.type == 'fixed'?require('../../assets/icons/fixed.png'):require('../../assets/icons/clock.png')}></Image>
                            <Text style={{fontWeight:'bold',marginTop:hp('1%'),...Style.jobdescription}}>{this.props.data.type == 'fixed'?"Fixed":"Hourly"}</Text>
                        </View>
                        <View style={style.item}>
                            <Image source={require('../../assets/icons/money.png')}></Image>
                            <Text style={{fontWeight:'bold',marginTop:hp('1%'),...Style.jobdescription}}>{this.props.data.amount} $</Text>
                        </View>
                    </View>
                    <View style={{marginTop:hp('4%'),display:'flex'}}>
                        <Text style={Style.jobtime}>{this.props.time}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const style = StyleSheet.create({
    joblayout:{
        paddingLeft:wp('5.6%'),
        paddingRight:wp('5.6%'),
        paddingTop:hp('1.25%'),
        paddingBottom:hp('1.25%'),
        flexDirection:'row',
        marginBottom:hp('1%')
    },
    review:{
        flexDirection:'row',
        marginBottom:hp('1.25%'),
        alignItems:'center'
    },
    description:{
        marginTop:hp('1.25%')
    },
    item:{
        marginRight:wp('1.5%'),
        justifyContent:'center',
        alignItems:'center'
    }
})
export default JobLayout;