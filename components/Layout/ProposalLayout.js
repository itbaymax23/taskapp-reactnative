import React from 'react';
import {View,ScrollView,SafeAreaView, Text,KeyboardAvoidingView,TouchableOpacity} from 'react-native';
import {Entypo} from '@expo/vector-icons';

import Style from '../style';

class ProposalLayout extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <KeyboardAvoidingView behavior="padding" style={Style.container}>
                <SafeAreaView style={Style.content}>
                    <View style={{...Style.top,alignItems:'center'}}>
                        {
                            !this.props.backdisable && (
                                <TouchableOpacity style={{position:'absolute',left:15}} onPress={()=>this.props.navigation.goBack()}>
                                    <Entypo name="chevron-thin-left" style={Style.back}></Entypo>
                                </TouchableOpacity>
                            )
                        }
                        
                        <Text style={{...Style.title1,marginLeft:'auto',marginRight:'auto'}}>{this.props.title}</Text>
                    </View>
                    <ScrollView style={Style.container}>
                        {this.props.children}
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}

export default ProposalLayout;