import {View} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import React from 'react';
import Style from '../style';

class AuthLayout extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return(
            <LinearGradient 
			start={[1,0]}
			end={[0,0]}
			locations={[1,0]} colors={['#4EA9B6','rgba(32, 59, 93, 0)']}  style={Style.container}>
                {this.props.children}
            </LinearGradient>
        )
    }
}

export default AuthLayout;