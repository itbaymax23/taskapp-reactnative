export {default as AuthLayout} from './Authlayout';
export {default as RegisterLayout} from './RegisterLayout';
export {default as HomeLayout} from './HomeLayout';
export {default as JobLayout} from './JobLayout';
export {default as ProposalLayout} from './ProposalLayout';