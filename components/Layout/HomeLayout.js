import React from 'react';
import {View,ScrollView,SafeAreaView,KeyboardAvoidingView} from 'react-native';
import Style from '../style';
import {SearchInput} from '../Element';

class HomeLayout extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    render()
    {
        return (
            <KeyboardAvoidingView style={Style.content}>
                <SafeAreaView style={Style.content}>
                    <View style={Style.top}>
                        <View style={{flex:1}}></View>
                        <View style={{flex:8}}>
                            <SearchInput placeholder={this.props.placeholder}></SearchInput>
                        </View>
                    </View>
                    {
                        this.props.title && this.props.title
                    }
                    <ScrollView style={Style.container}>
                        {this.props.children}
                    </ScrollView>
                </SafeAreaView>
            </KeyboardAvoidingView>
        )
    }
}

export default HomeLayout;