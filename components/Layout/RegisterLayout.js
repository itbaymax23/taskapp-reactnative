import React from 'react';
import {View,StyleSheet,KeyboardAvoidingView,TouchableOpacity,Text,ScrollView} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import {Ionicons} from '@expo/vector-icons';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Styles from '../style';


class RegisterLayout extends React.Component
{
    constructor(props)
    {
        super(props);
    }

    onPressback = () => {
        this.props.navigation.goBack();
    }
    render()
    {
        let body = JSON.parse(JSON.stringify(style.body));
        
        if(this.props.paddingdisabled)
        {
            body.paddingLeft = 0;
            body.paddingRight = 0;
        }

        return (
                <LinearGradient 
                start={[1,0]}
                end={[0,0]}
                locations={[1,0]} colors={['#4EA9B6','rgba(32, 59, 93, 0)']}  style={style.container}>
                     <KeyboardAvoidingView behavior="padding" style={Styles.container}>
                        <View style={Styles.container}>
                            <View style={style.statusbar}>
                                {
                                    this.props.back && (
                                        <TouchableOpacity onPress={this.onPressback}>
                                            <Ionicons  name="ios-arrow-back" style={style.arrow}></Ionicons>
                                        </TouchableOpacity>
                                    )
                                }
                                <Text style={style.title}>{this.props.title}</Text>
                            </View>
                        
                                <ScrollView style={body}>
                                    {this.props.children}
                                </ScrollView>
                            
                        </View>
                    </KeyboardAvoidingView>
                </LinearGradient>
        )
    }
}

const style = StyleSheet.create({
    container:{
        flex:1,
        paddingTop:hp('5%')
    },
    statusbar:{
        flexDirection:'row',
        alignItems:'center',
        paddingTop:hp('2%'),
        paddingBottom:hp('2%'),
        borderBottomColor:'#0822390C',
        borderBottomWidth:2,
        borderTopColor:'#0822390C',
        borderTopWidth:2
    },
    body:{
        flex:1,
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
    },
    arrow:{
        fontSize:hp('4%'),
        color:'#FFFFFF80',
        marginLeft:wp('5%')
    },
    title:{
        flex:1,
        textAlign:'center',
        color:'#F9F9F9',
        fontSize:hp('2.8%'),
        fontWeight:'bold',
        lineHeight:hp('3.5%'),
        fontFamily:"Arial"
    }
})
export default RegisterLayout;