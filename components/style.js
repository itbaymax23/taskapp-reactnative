import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';

const style = StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column'
    },
    content:{
        flex:1,
        backgroundColor:'#0822390E'
    },
    top:{
        height:hp('8%'),
        flexDirection:'row',
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        marginBottom:2,
        backgroundColor:'white',
        marginTop:hp('3%')
    },
    middle:{
        height:hp('10%'),
        flexDirection:'row',
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        marginBottom:2,
        backgroundColor:'white',
        alignItems:'center'
    },
    title2:{
        fontSize:hp('2.5%'),
        fontWeight:'bold',
        color:'#384D5F'
    },  
    button:{
        backgroundColor:'#FFFFFFF0',
        alignItems:'center',
        padding:hp('1.6%'),
        borderRadius:hp('3.2%'),
        height:hp('6.4%'),
        width:wp('94%'),
        flexDirection:'row',
        display:'flex'
    },
    buttontext_auth:{
        color:'black',
        fontSize:hp('2.5%'),
        fontWeight:'bold',
        textAlign:'center',
        marginLeft:'auto',
        marginRight:'auto',
        fontFamily:"Arial"
    },
    buttontext1:{
        color:'white',
        fontSize:hp('2.5%'),
        fontWeight:'bold',
        textAlign:'center',
        marginLeft:'auto',
        marginRight:'auto',
        fontFamily:"Arial"
    },
    progress_txt:{
        fontSize:hp('2%'),
        color:'white',
        fontWeight:'bold',
        fontFamily:'Arial'
    },
    defaulticon:{
        fontSize:hp('2.3%'),
        color:"#808E99"
    },
    icon:{
        width:hp('3.5%'),
        height:hp('3.5%'),
        marginLeft:wp('3%')
    },
    title1:{
        fontSize:hp('2.6%'),
        fontWeight:'bold',
        color:'#384D5F',
        fontFamily:'Arial'
    },
    jobtitle:{
        fontSize:hp('2.3%'),
        color:'#082239F0',
        fontWeight:'bold',
        fontFamily:'Arial'
    },
    description:{
        fontSize:hp('2%'),
        lineHeight:hp('2.4%'),
        color:'#808E99',
        fontFamily:'Arial'
    },
    description1:{
        fontSize:hp('2%'),
        lineHeight:hp('2.2%'),
        color:'#08223980'
    },
    jobdescription:{
        fontSize:hp('2%'),
        lineHeight:hp('2.8%'),
        color:'#082239B2',
        flexWrap:'wrap',
        fontFamily:'Arial'
    },
    jobtime:{
        fontWeight:'bold',
        color:'#08223980',
        opacity:0.4,
        fontFamily:'Arial'
    },
    star:{
        fontSize:hp('3%'),
        marginRight:5
    },
    back:{
        fontSize:hp('4%'),
        color:'#808E99'
    },
    defaulttext:{
        color:'#384D5F',
        fontWeight:'500',
        fontSize:hp('2.2%'),
        fontFamily:'Arial'
    },
    covertext:{
        flex:1,
        borderRadius:5,
        borderColor:'#384D5F',
        borderWidth:1,
        flexDirection:'row',
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:5,
        paddingBottom:5,
        fontFamily:'Arial'
    },
    locationtext:{
        fontSize:hp('2.2%'),
        color:'#08223980',
        fontFamily:'Arial'
    },
    locationpin:{
        fontSize:hp('3.5%'),
        color:'#4EA9B6'
    },
    card:{
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('2%'),
        paddingBottom:hp('2%'),
        marginBottom:2,
        backgroundColor:'white'
    },
    carditem:{
        paddingLeft:wp('3%'),
        paddingRight:wp('3%'),
        paddingTop:hp('1%'),
        paddingBottom:hp('1%'),
        backgroundColor:'white'
    },
    todoitem_text:{
        color:'#384D5F',
        fontSize:hp('2%'),
        fontWeight:'bold',
        fontFamily:"Arial"
    },
    todoitem_close:{
        color:'#384D5F',
        fontSize:hp('2.6%'),
        fontWeight:'bold',
        marginLeft:wp('6.4%')
    },
    todoitem:{
        paddingLeft:wp('4%'),
        paddingRight:wp('4%'),
        paddingTop:hp('1.25%'),
        paddingBottom:hp('1.25%'),
        backgroundColor:'white',
        flexDirection:'row',
        marginRight:wp('3%'),
        alignItems:'center',
        marginTop:hp('1%'),
        borderRadius:4
    }
})

export default style;